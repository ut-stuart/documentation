all: CourierFlowcharts.pdf flowcharts.pdf

clean:
	rm -f *.pdf

%.pdf : %.odg
	unoconv -f pdf -o $@ $<

flowcharts.pdf: flowcharts.tex
	pdflatex $< -o $@

